"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv/config");
const express_1 = __importDefault(require("express"));
const http_1 = __importDefault(require("http"));
const cors_1 = __importDefault(require("cors"));
const socket_io_1 = require("socket.io");
const app = (0, express_1.default)();
const httpServer = http_1.default.createServer(app);
const io = new socket_io_1.Server(httpServer);
const port = process.env.PORT;
const host = process.env.HOST;
io.on('connection', (socket) => {
    console.log('user connected on' + socket.id);
    socket.on('disconnect', () => {
        console.log('user disconnected on' + socket.id);
    });
    socket.join("room_1");
    socket.in("room_1").emit("ping");
    socket.emit("test", "ini adalah message tes");
    console.log(io.engine.clientsCount);
});
app.use((0, cors_1.default)());
app.get('/', (req, res) => {
    res.send(`<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Socket.IO</title>
    </head>
    <body>
        <div style="width: 200px; height: 200px; border: 2px solid black">
        </div>
        <input type='text' id="text" placeholder="kirim chat...">
        <button onclick="kirim()"> Kirim </button>
    </body>
    </html>`);
});
app.post('/', (req, res) => {
    console.log("post terkirim");
});
app.post("/webhook", (req, res, next) => {
    console.log('asd');
});
httpServer.listen(port, () => {
    console.log(`Server running at ${host}:${port}/`);
});
