import 'dotenv/config'
import express from "express";
import http from "http";
import cors from "cors";
import { Server } from 'socket.io';

const app = express();
const httpServer = http.createServer(app);

const io = new Server(httpServer);

const port = process.env.PORT
const host = process.env.HOST

io.on('connection', (socket) => {
    console.log('user connected on' + socket.id)
    socket.on('disconnect', () => {
        console.log('user disconnected on' + socket.id);
    });
    socket.join("room_1");
    socket.in("room_1").emit("ping");
    socket.emit("test", "ini adalah message tes")
    console.log(io.engine.clientsCount)
})

app.use(cors());

app.get('/', (req, res) => {
    res.send(`<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Socket.IO</title>
    </head>
    <body>
        <div style="width: 200px; height: 200px; border: 2px solid black">
        </div>
        <input type='text' id="text" placeholder="kirim chat...">
        <button onclick="kirim()"> Kirim </button>
    </body>
    </html>`)
})

app.post('/', (req, res) => {
    console.log("post terkirim")
})

app.post("/webhook", (req, res, next) => {
    console.log('asd')
})

httpServer.listen(port, () => {
    console.log(`Server running at ${host}:${port}/`);
});