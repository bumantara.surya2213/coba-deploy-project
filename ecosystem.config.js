module.exports = {
    apps: {
        name: 'coba-websocket-main',
        script: 'dist/index.js',
        watch: false,
        env: {
            PORT: 3501,
            HOST: 'http://127.0.0.1',
        }
    }
}